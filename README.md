Module Ansible vSphere
============

Module Ansible pour vSphere

Installation
============

Via pip, compresser le contenu dans une archive zip afin de l'installer.
```
pip install ansible-module-vSphere.zip

```

Via setup.py.
```
python setup.py install
```
Example de playbook
=====================

# Eteindre une Machine Virtuelle

```
    - name: Eteindre VM
      vmware:
        hostname: 192.168.1.110
        username: administrator@vsphere.local
        password: secret
        vm: debian9_lab01
        state: poweredoff
        force: False
```      

# Récupère l'état d'une VM

```
    - name: Récupère l'état d'une VM
      vmware:
        hostname: 192.168.1.110
        username: administrator@vsphere.local
        password: secret
        vm: debian9_lab01
        state: status
        force: False
```