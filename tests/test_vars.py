vcenterok = '192.168.1.100'
vcenternok = '192.0.0.0'
username = 'root'
password = 'ertyerty'


moduleTESTOK = AnsibleModule(
        argument_spec=dict(
            hostname=dict(
                type='str',
                default=vcenterok
            ),
            username=dict(
                type='str',
                default=username
            ),
            password=dict(
                type='str', no_log=True,
                default=password
            ),
            state=dict(required=True, type='str', default='present'),
            force=dict(required=False, type='bool', default=False),
            vm=dict(required=True, type='str', default='mangas')
        )
    )
moduleTESTVCSNOK = AnsibleModule(
        argument_spec=dict(
            hostname=dict(
                type='str',
                default=vcenternok
            ),
            username=dict(
                type='str',
                default=username
            ),
            password=dict(
                type='str', no_log=True,
                default=password
            ),
            state=dict(required=True, type='str', default='present'),
            force=dict(required=False, type='bool', default=False),
            vm=dict(required=True, type='str', default='mangas')
        )
    )
moduleTESTBADVMENCODE = AnsibleModule(
        argument_spec=dict(
            hostname=dict(
                type='str',
                default=vcenterok
            ),
            username=dict(
                type='str',
                default=username
            ),
            password=dict(
                type='str', no_log=True,
                default=password
            ),
            state=dict(required=True, type='str', default='present'),
            force=dict(required=False, type='bool', default=False),
            vm=dict(required=True, type='str', default='@--1851"')
        )
    )
moduleTESTBADSTATE = AnsibleModule(
        argument_spec=dict(
            hostname=dict(
                type='str',
                default=vcenterok
            ),
            username=dict(
                type='str',
                default=username
            ),
            password=dict(
                type='str', no_log=True,
                default=password
            ),
            state=dict(required=True, type='str', default='vbzekrb'),
            force=dict(required=False, type='bool', default=False),
            vm=dict(required=True, type='str', default='mangas')
        )
    )