from unittest import TestCase

from ansible.module_utils.basic import *

from modules.vmware_stig import *


class TestVsphereObject(TestCase):
    def test_connexion_ok(self):
        obj = VsphereObject(moduleTESTOK)
        if obj:
            print "TEST CONNEXION : OK"
        else:
            self.fail()

    def test_connexion_failed(self):
        obj = VsphereObject(moduleTESTVCSNOK)
        if obj:
            print "TEST CONNEXION : FAILED"
        else:
            self.fail()
