#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup
import os
import glob
import shutil
import sys
from setuptools.command.install import install as _install

racine = os.path.abspath(os.path.dirname(__file__))


def read(fichier):
    with open(os.path.join(os.path.dirname(__file__), fichier)) as fn:
        return fn.read()

with open('requirements.txt') as f:
    required = f.read().splitlines()


def _post_install(msg):
    """
    Install le(s) module(s) dans ansible
    """
    try:
        from ansible.constants import DEFAULT_MODULE_PATH
    except ImportError:
        print
        print("Répertoire d'installation d'ansible introuvable")
        print("Il vous faudra copier manuellement le module")
        sys.exit(1)

    if not DEFAULT_MODULE_PATH:
        print("Aucun répertoire par défaut des modules, configurez-le dans le fichier ansible.cfg")
        print("Ou alors copiez manuellement le/les modules dans le répertoire des modules ansible")
        sys.exit(1)

    module_dir = os.path.join(racine, "modules")
    modules = glob.glob(module_dir+"/*")

    vmware_modules_dir = os.path.join(DEFAULT_MODULE_PATH[0].encode('utf-8'), "stig")
    if not os.path.isdir(vmware_modules_dir):
        os.mkdir(vmware_modules_dir)

    for module in modules:
        shutil.copy(module, vmware_modules_dir)

    print
    print("Copie des modules ansible terminée")


class install(_install):
    def run(self):
        _install.run(self)
        self.execute(_post_install, ("",), msg="")


def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='ansible-modules-vSphere',
    version='0.2',
    description='Modules Ansible vSphere',
    long_description=readme(),
    url='https://framagit.org/tamashii/ansible-modules-vSphere',
    author='LE CESNE Jean-Baptiste',
    author_email='mr.lecesne@gmail.com',
    license='Apache License, Version 2.0',
    classifiers=[
        "Programming Language :: Python"
        "Programming Language :: Python :: 2.7",
        "Development Status :: 1 - dev",
        "License :: Apache License",
        "Natural Language :: French",
        "Operating System :: OS Independent"

    ],
    keywords='pyvmomi vsphere vmware esx',
    packages=[''],
    platforms=['Windows', 'Linux', 'Solaris', 'Mac OS-X', 'Unix'],
    cmdclass={'install': install},
    install_requires=required,
    zip_safe=False
)
