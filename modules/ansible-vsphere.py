#!/usr/bin/env python
# -*- coding: utf-8 -*-

ANSIBLE_METADATA = {'status': ['preview'],
                    'supported_by': 'LE CESNE Jean-Baptiste',
                    'version': '0.1'}


DOCUMENTATION = '''
---
module: vmware
short_description: Agir sur Vcenter/Esxi avec Ansible
description:
    - Basé sur Pyvmomi, inspiré du module vmware_guest
    - poweron/poweroff/restart une machine virtuelle
    - recupère l'état d'une machine virtuelle
version_added: 0.1
author: LE CESNE Jean-Baptiste
notes:
    - Testé sur vSphere 6.5
requirements:
    - "python >= 2.6"
    - "PyVmomi >= 6.5"
options:
   state:
   		description:
            - Etat désiré de la machine virtuelle
        required: True
        choices: ['status', 'poweredon', 'poweredoff', 'restart']
   vm:
        description:
            - Hostname de la machine virtuelle cible
        required: True
   force:
        description:
            - Indique si l'on souhaite agir en force (utiliser avec précaussion)
        required False
   hostname:
        description:
            - FQDN/IP du Vcenter/Esxi
        required True
   username:
        description:
            - Compte avec les droits nécéssaires sur le Vcenter/Esxi
        required True
   password:
        description:
            - Mot de passe du compte utilisateur 
        required True
    
'''
EXAMPLES = '''
Example from Ansible playbook
#
# Eteindre une Machine Virtuelle
#
    - name: Eteindre VM
      vmware:
        hostname: 192.168.1.110
        username: administrator@vsphere.local
        password: secret
        vm: debian9_lab01
        state: poweredoff
        force: False
        
Example from Ansible playbook
#
# Récupère l'état d'une VM
#
    - name: Récupère l'état d'une VM
      vmware:
        hostname: 192.168.1.110
        username: administrator@vsphere.local
        password: secret
        vm: debian9_lab01
        state: status
        force: False
      register: resultat
    - debug: msg="{{resultat.state}}"
        
'''
RETURN = """
state:
    descripton: l'état de la VM
    returned: always
    type: str
    sample: None
"""

# LIBRAIRIES
import atexit
import ssl
import os
try:
    from pyVim.connect import SmartConnect, Disconnect
    import pyVmomi
    from pyVmomi import vim, vmodl
    HAS_PYVMOMI = True
except ImportError:
    HAS_PYVMOMI = False
    pass

from ansible.module_utils.basic import *
# CLASSES


class VsphereObject(object):
    def __init__(self, module):
        if not HAS_PYVMOMI:
            module.fail_json(msg='pyvmomi >= 6.5 est nécéssaire !')

        self.module = module
        self.params = module.params
        self.si = None
        self.connexion()
        self.vm = self.get_vm()
        self.powerstate = None
        self.content = None

    def connexion(self):

        session = None
        try:
            context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
            context.verify_mode = ssl.CERT_NONE
            session = SmartConnect(host=self.module.params['hostname'], user=self.module.params['username'],
                                   pwd=self.module.params['password'], port=443, sslContext=context)
        except IOError:
            pass

        if not session:
            self.module.fail_json(msg="Erreur de connexion, vérifiez vos credentials !")
            return -1

        atexit.register(Disconnect, session)

        session.RetrieveContent()
        self.content = session.RetrieveContent()

    def get_vm(self):

        hostname = None
        try:
            hostname = unicode(self.module.params['vm'], 'utf-8')
        except TypeError:
            self.module.fail_json(msg="Hostname de la VM non conforme !")

        vm = None
        container = self.content.viewManager.CreateContainerView(
            self.content.rootFolder, [vim.VirtualMachine], True)

        for c in container.view:
            if c.name == hostname:
                vm = c
                break
        self.vm = vm
        return vm

    def get_powerstate(self):

        return self.vm.summary.runtime.powerState

    def is_poweredon(self):

        powerstate = self.vm.summary.runtime.powerState
        if powerstate == 'poweredOn':
            return True
        else:
            return False

    def set_powerstate(self, state, force):
        if state in ['poweredon', 'poweredoff', 'restart']:
            if state == 'poweredon':
                if self.is_poweredon() is True:
                    self.module.fail_json(msg="La VM est déjà démarrée !")
                    return {'changed': False, 'failed': True, 'vm': self.vm.name}
                try:
                    self.vm.PowerOnVM_Task()
                except vim.fault.TaskInProgress:
                    self.module.fail_json(msg="La VM a déjà une tâche en cours")
                except vim.fault.InvalidState:
                    self.module.fail_json(msg="Esx en maintenance ou VM non conforme")
                except vim.fault.InsufficientResourcesFault:
                    self.module.fail_json(msg="Une politique de réservation de resource empêche le démarrage de la VM")
                except vim.fault.FileFault:
                    self.module.fail_json(msg="Accès aux fichiers de configuration de la VM impossible")
                except vim.InvalidPowerState:
                    self.module.fail_json(msg="La VM est déjà démarrée")
                    return {'changed': False, 'failed': True, 'instance': self.vm.name}
                except vmodl.fault.NotEnoughLicenses:
                    self.module.fail_json(msg="Problème de licencing, démarrage impossible de la VM")
                except vmodl.fault.NotSupported:
                    self.module.fail_json(msg="La VM est un template")
                except vim.fault.DisallowedOperationOnFailoverHost:
                    self.module.fail_json(msg="L'ESX n'est pas disponible")
                except vim.fault.NoPermission:
                    self.module.fail_json(msg="Permissions inssuffisantes pour effectuer cette tâche")
                except Exception as e:
                    self.module.fail_json(msg="%s, %s" % (e.message, e.args))

                return {'changed': True, 'failed': False, 'vm': self.vm.name, 'action': 'PoweredOn'}

            if state == 'poweredoff':
                if self.is_poweredon() is False:
                    self.module.fail_json(msg="La VM est déjà éteinte !")
                    return {'changed': False, 'failed': True, 'vm': self.vm.name}
                if force is False:
                    try:
                        self.vm.ShutdownGuest()
                    except vim.fault.ToolsUnavailable:
                        self.module.fail_json(msg="Les VMware Tools ne sont pas démarrés")
                    except vim.fault.TaskInProgress:
                        self.module.fail_json(msg="La VM a déjà une tâche en cours")
                    except vim.fault.InvalidPowerState:
                        self.module.fail_json(msg="La VM est déjà éteinte ou en pause")
                    except vim.fault.NoPermission:
                        self.module.fail_json(msg="Permissions inssuffisantes pour effectuer cette tâche")
                    except Exception as e:
                        self.module.fail_json(msg="%s, %s" % (e.message, e.args))

                    return {'changed': True, 'failed': False, 'vm': self.vm.name, 'action': 'PoweredOff'}
                else:
                    try:
                        self.vm.PowerOff()
                    except vmodl.fault.NotSupported:
                        self.module.fail_json(msg="La VM est un template")
                    except vim.fault.TaskInProgress:
                        self.module.fail_json(msg="La VM a déjà une tâche en cours")
                    except vim.fault.InvalidPowerState:
                        self.module.fail_json(msg="La VM est déjà éteinte ou en pause")
                    except vim.fault.NoPermission:
                        self.module.fail_json(msg="Permissions inssuffisantes pour effectuer cette tâche")
                    except Exception as e:
                        self.module.fail_json(msg="%s, %s" % (e.message, e.args))

                    return {'changed': True, 'failed': False, 'vm': self.vm.name, 'action': 'PoweredOff (forcé)'}

            if state == 'restart':
                if force is False:
                    if self.is_poweredon() is False:
                        self.module.fail_json(msg="La VM est éteinte !")
                        return {'changed': False, 'failed': True, 'vm': self.vm.name}
                    try:
                        self.vm.RebootGuest()
                    except vim.fault.ToolsUnavailable:
                        self.module.fail_json(msg="Les VMware Tools ne sont pas démarrés")
                    except vim.fault.TaskInProgress:
                        self.module.fail_json(msg="La VM a déjà une tâche en cours")
                    except vim.fault.InvalidPowerState:
                        self.module.fail_json(msg="La VM est déjà éteinte ou en pause")
                    except vim.fault.NoPermission:
                        self.module.fail_json(msg="Permissions inssuffisantes pour effectuer cette tâche")
                    except Exception as e:
                        self.module.fail_json(msg="%s, %s" % (e.message, e.args))

                    return {'changed': True, 'failed': False, 'vm': self.vm.name, 'action': 'Restart'}

                else:
                    try:
                        self.vm.Reset()
                    except vim.fault.TaskInProgress:
                        self.module.fail_json(msg="La VM a déjà une tâche en cours")
                    except vim.fault.InvalidState:
                        self.module.fail_json(msg="Esx en maintenance ou VM non conforme")
                    except vim.InvalidPowerState:
                        self.module.fail_json(msg="La VM est déjà démarrée")
                    except vmodl.fault.NotEnoughLicenses:
                        self.module.fail_json(msg="Problème de licencing, démarrage impossible de la VM")
                    except vmodl.fault.NotSupported:
                        self.module.fail_json(msg="La VM est un template")
                    except vim.fault.NoPermission:
                        self.module.fail_json(msg="Permissions inssuffisantes pour effectuer cette tâche")
                    except Exception as e:
                        self.module.fail_json(msg="%s, %s" % (e.message, e.args))

                    return {'changed': True, 'failed': False, 'vm': self.vm.name, 'action': 'Restart (forcé)'}
        else:
            self.module.fail_json(msg="Etat de la VM demandé inexistant !")


def main():

    module = AnsibleModule(
        argument_spec=dict(
            hostname=dict(
                type='str',
                default=os.environ.get('VMWARE_HOST')
            ),
            username=dict(
                type='str',
                default=os.environ.get('VMWARE_USER')
            ),
            password=dict(
                type='str', no_log=True,
                default=os.environ.get('VMWARE_PASSWORD')
            ),
            state=dict(
                required=True,
                choices=[
                    'poweredon',
                    'poweredoff',
                    'status',
                    'restart',
                ]),
            force=dict(required=False, type='bool', default=False),
            vm=dict(required=True, type='str')
        )
    )

    obj = VsphereObject(module)

    if module.params['state'] == 'status':
        module.exit_json(state=obj.get_powerstate())
    else:
        resultat = obj.set_powerstate(module.params['state'], module.params['force'])
        module.exit_json(**resultat)


if __name__ == '__main__':
    main()
